using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameUIHandler : MonoBehaviour
{
    [SerializeField]
    GameObject GameOverScreen;

    [SerializeField]
    TMP_Text ScoreText;

    [SerializeField]
    TMP_Text CoinsText;

    [SerializeField]
    TMP_Text HighScoreText;

    [SerializeField]
    Button MenuButton;

    private void Awake()
    {
        MenuButton.onClick.AddListener(LoadMenu);
    }

    public void ShowScreen()
    {
        PersistenceManager.Instance.TryUpdateHighScore(GameManager.Instance.Score);
        ScoreText.text = $"Score: {GameManager.Instance.Score}";
        HighScoreText.text = $"HighScore: {PersistenceManager.Instance.HighScore}";
        CoinsText.text = $"Coins Earned: {PersistenceManager.Instance.AddCoinsFromScore(GameManager.Instance.Score)}";
        GameOverScreen.SetActive(true);
    }

    private void LoadMenu()
    {
        SceneManager.LoadScene("Title");
    }



}
