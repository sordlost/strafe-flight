using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Controls Config", menuName = "Scriptable Objects/ControlsConfig", order = 2)]
public class ControlsConfig : ScriptableObject
{
    [SerializeField]
    bool invertYAxis;

    public bool InvertYAxis { get => invertYAxis; }
}
