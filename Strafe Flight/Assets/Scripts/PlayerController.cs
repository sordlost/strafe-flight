using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    Mobile.Controls.JoystickController joystickController;

    [SerializeField]
    Mobile.GesturesController gesturesController;

    [SerializeField]
    ControlsConfig controlsConfig;

    [SerializeField]
    FlyerController flyerController;

    [Range(-1.0f, 1.0f)]
    public float XInput;
    [Range(-1.0f, 1.0f)]
    public float YInput;

    List<Mobile.Gesture> activeGestures = new List<Mobile.Gesture>();

    private void OnEnable()
    {
        gesturesController.OnGestureStarted += OnGestureStartedHandler;
        flyerController.OnAccAbilityComplete += OnAbilityPerformedHandler;
    }

    private void OnGestureStartedHandler(Mobile.Gesture gesture)
    {
        activeGestures.Add(gesture);
        flyerController.StartAbilityAccelerate();
    }

    private void OnAbilityPerformedHandler(string actionName)
    {
        switch (actionName)
        {
            case "Accelerate":
                Mobile.Gesture gesture = activeGestures.First(x => x.GetType() == typeof(Mobile.AccelerateSwipe));
                gesturesController.TriggerGestureExecuted(gesture);
                break;
        }
    }

    public void Update()
    {
        XInput = joystickController.XOffset;
        YInput = controlsConfig.InvertYAxis ? -joystickController.YOffset : joystickController.YOffset;
    }
}
