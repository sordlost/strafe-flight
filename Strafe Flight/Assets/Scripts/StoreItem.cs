using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Store Item", menuName = "Scriptable Objects/StoreItem", order = 1)]
public class StoreItem : ScriptableObject
{
    [SerializeField]
    private string itemName;

    [SerializeField]
    private int price;

    public string Name { get => itemName; }
    public int Price { get => price; }
}
