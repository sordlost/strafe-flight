using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseCam : MonoBehaviour
{
    [SerializeField]
    Transform target;

    [SerializeField]
    Vector3 offset;

    [SerializeField]
    float zDamping;

    float zPos;

    private void FixedUpdate()
    {
        Vector3 targetPos = target.position + offset;
        float nextZ = Mathf.SmoothDamp(transform.position.z, targetPos.z, ref zPos, zDamping);
        targetPos.z = nextZ;
        transform.position = targetPos;
    }
}
