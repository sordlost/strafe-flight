using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorldManager : MonoBehaviour
{
    private WorldConfig worldConfig;

    private GameObject playerObject;

    private List<Obstacle> activeObstacles = new List<Obstacle>();

    private Vector3 playerPosition;

    private Vector3 lastSpawnPos;

    private void Start()
    {
        playerObject = GameManager.Instance.PlayerObject;
        worldConfig = GameManager.Instance.ActiveWorldConfig;
        GenerateBatch(true);
    }

    private void Update()
    {
        playerPosition = playerObject.transform.position;
        // this is temporary
        if (activeObstacles.Count < worldConfig.ObstacleBatchSize / 1.1)
        {
            GenerateBatch(false);
        }

        if (playerObject.transform.position.z > worldConfig.ReRootInterval)
        {
            ReRoot();
        }
    }

    private void DestroyObstacle(Obstacle obstacle)
    {
        obstacle.OnObstacleCrossed -= DestroyObstacle;
        activeObstacles.Remove(obstacle);
        Destroy(obstacle.gameObject);
    }

    private void GenerateBatch(bool initial)
    {
        Vector3 obstacleSpawnPos = initial ? new Vector3(0.0f, 0.0f, playerPosition.z + worldConfig.ObstacleSpawnRange.x) : new Vector3(0.0f, 0.0f, lastSpawnPos.z);
        for (int i = 0; i < worldConfig.ObstacleBatchSize; i++)
        {
            GameObject obstaclePrefab = worldConfig.Obstacles[UnityEngine.Random.Range(0, worldConfig.Obstacles.Count)];
            obstacleSpawnPos.z += UnityEngine.Random.Range(worldConfig.ObstacleSpawnRange.x, worldConfig.ObstacleSpawnRange.y);
            lastSpawnPos = obstacleSpawnPos;
            GameObject obstacleGO = Instantiate(obstaclePrefab, obstacleSpawnPos, Quaternion.identity);
            Obstacle obstacle = obstacleGO.GetComponent<Obstacle>();


            if (obstacle == null)
            {
                Debug.LogError("No Obstacle component attached to the obstacle GameObject!", obstacleGO);
            }

            obstacle.OnObstacleCrossed += DestroyObstacle;

            activeObstacles.Add(obstacle);
        }
    }

    private void ReRoot()
    {
        foreach (var obstacle in activeObstacles)
        {
            Vector3 nextPos = obstacle.transform.position;
            nextPos.z = obstacle.transform.position.z - worldConfig.ReRootInterval;
            obstacle.transform.position = nextPos;
        }

        Vector3 playerNextPos = playerObject.transform.position;
        playerNextPos.z = playerObject.transform.position.z - worldConfig.ReRootInterval;
        playerObject.transform.position = playerNextPos;

        Vector3 cameraNextPos = GameManager.Instance.MainCamera.transform.position;
        cameraNextPos.z = cameraNextPos.z - worldConfig.ReRootInterval;
        GameManager.Instance.MainCamera.transform.position = cameraNextPos;

        lastSpawnPos = activeObstacles[activeObstacles.Count - 1].transform.position;
    }
}
