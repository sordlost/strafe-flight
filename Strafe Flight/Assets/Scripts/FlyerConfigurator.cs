using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyerConfigurator : MonoBehaviour
{
    #region Singleton Definition
    private static FlyerConfigurator instance;

    public static FlyerConfigurator Instance { get => instance; }

    private FlyerConfigurator()
    {

    }
    #endregion

    [System.Serializable]
    private struct FlyerItemContainer
    {
        public StoreItem storeItem;
        public Flyer flyer;
    }

    [System.Serializable]
    private struct ColorItemContainer
    {
        public StoreItem storeItem;
        public Color color;
    }

    [System.Serializable]
    private struct PrefabItemContainer
    {
        public StoreItem storeItem;
        public GameObject prefab;
    }

    [SerializeField]
    private List<FlyerItemContainer> flyerDatabase;

    [SerializeField]
    private List<ColorItemContainer> colorDatabase;

    [SerializeField]
    private List<PrefabItemContainer> prefabDatabase;

    [SerializeField]
    private StoreItem selectedFlyer;

    [SerializeField]
    private StoreItem selectedColor;

    public Flyer ActiveFlyer { get => flyerDatabase.Find(x => x.storeItem == ActiveFlyerItem).flyer; }

    public Color ActiveColor { get => colorDatabase.Find(x => x.storeItem == ActiveColorItem).color; }

    public GameObject ActivePrefab { get => prefabDatabase.Find(x => x.storeItem == selectedFlyer).prefab; }

    public GameObject PreviewPrefab { get => prefabDatabase.Find(x => x.storeItem == PreviewFlyer).prefab; }

    public StoreItem ActiveFlyerItem { get => selectedFlyer; private set => selectedFlyer = value; }

    public StoreItem ActiveColorItem { get => selectedColor; private set => selectedColor = value; }

    public StoreItem PreviewFlyer { get => flyerDatabase[previewFlyerIdx].storeItem; }

    int previewFlyerIdx = 0;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this);
    }

    public void SetNewColor(StoreItem item)
    {
        ActiveColorItem = item;
    }

    public void SetNewFlyer(StoreItem item)
    {
        ActiveFlyerItem = item;
    }

    public bool NextPreviewFlyer(bool next)
    {
        if (next)
        {
            if (previewFlyerIdx < flyerDatabase.Count - 1)
            {
                previewFlyerIdx++;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (previewFlyerIdx > 0)
            {
                previewFlyerIdx--;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public void FlyerSwitchColor(GameObject flyerObject)
    {
        foreach (var renderer in flyerObject.GetComponentsInChildren<MeshRenderer>())
        {
            foreach (var material in renderer.materials)
            {
                if (material.name.Contains("Purple"))
                {
                    material.SetColor("_BaseColor", ActiveColor);
                }
            }
        }
    }

    public void FlyerSwitchObject(GameObject flyerObject, bool preview)
    {
        foreach (var tr in flyerObject.GetComponentsInChildren<Transform>())
        {
            if (tr.name == "Flyer")
            {
                Destroy(tr.GetChild(0).gameObject);
                var prefab = preview ? PreviewPrefab : ActivePrefab;
                GameObject flyerGo = GameObject.Instantiate(prefab, tr.position, Quaternion.identity);
                flyerGo.transform.parent = tr;
            }
        }
    }

    public void FlyerSwitchConfig(GameObject flyerObject)
    {
        FlyerController flyerController = flyerObject.GetComponentInChildren<FlyerController>();
        flyerController.flyer = ActiveFlyer;
    }

}
