using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class FlyerController : MonoBehaviour
{
    public Flyer flyer;

    [SerializeField]
    PlayerController playerController;

    [SerializeField]
    WorldConfig worldConfig;

    [SerializeField]
    AbilitiesConfig abilitiesConfig;

    public event Action OnFlyerDestroyed;
    public event Action<string> OnAccAbilityComplete;

    float acceleration;

    float maxVelocityForward;

    new Rigidbody rigidbody;

    float worldLimX;
    float worldLimY;

    bool isAlive = true;

    private void OnEnable()
    {
        rigidbody = gameObject.GetComponent<Rigidbody>();
        rigidbody.mass = flyer.Mass;
        worldLimX = worldConfig.WorldBounds.x / 2.0f;
        worldLimY = worldConfig.WorldBounds.y / 2.0f;
        maxVelocityForward = flyer.MaxVelocityForward;
    }

    private void FixedUpdate()
    {
        if (isAlive)
        {
            acceleration = flyer.DampingVelZ;

            float inputX = playerController.XInput;
            float inputY = -playerController.YInput;

            inputX = rigidbody.position.x <= -worldLimX && inputX < 0.0f ? 0.0f : inputX;
            inputX = rigidbody.position.x >= worldLimX && inputX > 0.0f ? 0.0f : inputX;
            inputY = rigidbody.position.y <= -worldLimY && inputY < 0.0f ? 0.0f : inputY;
            inputY = rigidbody.position.y >= worldLimY && inputY > 0.0f ? 0.0f : inputY;

            ApplyYawRoll(inputX, inputY);
            ApplyVelocity(inputX, inputY);

            Debug.DrawRay(rigidbody.transform.position, rigidbody.transform.forward * 100.0f, Color.red);
            Debug.DrawRay(rigidbody.transform.position, rigidbody.velocity.normalized * 100.0f, Color.green);
        }
    }

    #region Reference Angle Fields
    float xAng;
    float yAng;
    float zAng;
    #endregion

    private void ApplyYawRoll(float inputX, float inputY)
    {
        Quaternion rotation = Quaternion.FromToRotation(rigidbody.transform.forward, rigidbody.velocity.normalized);
        Vector3 targetRot = rigidbody.transform.rotation.eulerAngles + rotation.eulerAngles;
        float targetRotX = Mathf.SmoothDampAngle(rigidbody.rotation.eulerAngles.x, targetRot.x, ref xAng, flyer.DampingRotX);
        float targetRotZ = Mathf.SmoothDampAngle(rigidbody.rotation.eulerAngles.z, -inputX * flyer.TurningBounds.z, ref zAng, flyer.DampingRotZ);
        rigidbody.rotation = Quaternion.Euler(targetRotX, 0.0f, targetRotZ);
    }

    #region Reference Velocity Fields
    float xVel;
    float yVel;
    float zVel;
    #endregion

    private void ApplyVelocity(float inputX, float inputY)
    {
        float targetVelocityX = flyer.MaxVelocityHorizontal * inputX * rigidbody.velocity.z / maxVelocityForward;
        float targetVelocityY = flyer.MaxVelocityVertical * inputY * rigidbody.velocity.z / maxVelocityForward;

        float velX = Mathf.SmoothDamp(rigidbody.velocity.x, targetVelocityX, ref xVel, flyer.DampingVelX);
        float velY = Mathf.SmoothDamp(rigidbody.velocity.y, targetVelocityY, ref yVel, flyer.DampingVelY);
        float velZ = Mathf.SmoothDamp(rigidbody.velocity.z, maxVelocityForward, ref zVel, acceleration);

        rigidbody.velocity = new Vector3(velX, velY, velZ);
    }

    public void StartAbilityAccelerate()
    {
        StartCoroutine(AbilityAccelerate());
    }

    private IEnumerator AbilityAccelerate()
    {
        maxVelocityForward += abilitiesConfig.AccVelocityBoost;
        float timer = abilitiesConfig.AccTimespan;
        while (timer > 0.0f)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        maxVelocityForward -= abilitiesConfig.AccVelocityBoost;
        OnAccAbilityComplete?.Invoke("Accelerate");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("KillPlayer"))
        {
            OnFlyerDestroyed?.Invoke();
            rigidbody.velocity = Vector3.zero;
            isAlive = false;
        }
    }
}
