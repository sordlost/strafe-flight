using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PersistenceManager : MonoBehaviour
{
    const string DEFAULT_FLYER_TEXT = "DefaultFlyer";
    const string DEFAULT_COLOR_TEXT = "Purple";
    const string HIGHSCORE_TEXT = "HighScore";
    const string COINS_TEXT = "Coins";

    #region Singleton Definition
    private static PersistenceManager instance;

    public static PersistenceManager Instance { get => instance; }

    private PersistenceManager()
    {

    }
    #endregion

    [SerializeField]
    int pointsDivider;

    public int scoreMultiplier;

    public int Coins { get => PlayerPrefs.GetInt(COINS_TEXT); private set => PlayerPrefs.SetInt(COINS_TEXT, value); }

    public int HighScore { get => PlayerPrefs.GetInt(HIGHSCORE_TEXT); private set => PlayerPrefs.SetInt(HIGHSCORE_TEXT, value); }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        InitialSetup();
        DontDestroyOnLoad(this);
    }

    public int AddCoinsFromScore(int score)
    {
        int amount = score / pointsDivider;
        Coins += amount;
        return amount;
    }

    public void UnlockFlyer(string flyerName)
    {
        PlayerPrefs.SetString(flyerName, "UNLOCKED");
    }

    public void UnlockColor(string colorName)
    {
        PlayerPrefs.SetString(colorName, "UNLOCKED");
    }

    public bool TryPurchase(int price)
    {
        if (Coins >= price)
        {
            Coins -= price;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool TryUpdateHighScore(int score)
    {
        if (HighScore < score)
        {
            HighScore = score;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckItemUnlocked(string itemName)
    {
        if (PlayerPrefs.GetString(itemName) == string.Empty)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private void InitialSetup()
    {
        //PlayerPrefs.DeleteAll();

        if (PlayerPrefs.GetString(DEFAULT_FLYER_TEXT) == string.Empty)
        {
            UnlockFlyer(DEFAULT_FLYER_TEXT);
        }

        if (PlayerPrefs.GetString(DEFAULT_COLOR_TEXT) == string.Empty)
        {
            UnlockColor(DEFAULT_COLOR_TEXT);
        }

        if (PlayerPrefs.GetInt(COINS_TEXT) == 0)
        {
            PlayerPrefs.SetInt(COINS_TEXT, 10000);
        }
    }
}
