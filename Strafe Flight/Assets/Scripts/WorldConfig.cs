using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "World Config", menuName = "Scriptable Objects/World Config", order = 1)]
public class WorldConfig : ScriptableObject
{
    // this might later on be implemented as a <float, GameObject> dictionary
    // with the key marking the difficulty and the value holding the obstacle
    [SerializeField]
    private List<GameObject> obstacles;

    // world bounds defined by an X,Y axis
    // this value should be used both to limit the player's movement range
    // and to position the instantiated obstacles by the obstacle generator
    [SerializeField]
    private Vector2Int worldBounds;

    // how far apart from each other should the obstacles spawn
    // this might later on be implemented as a <float, Vector2> dictionary
    // with the key marking the difficulty and the value holding the range
    [SerializeField]
    private Vector2 obstacleSpawnRange;

    // after what distance traveled should we re-root the world and the player
    // this might go to a different config
    [SerializeField]
    private float reRootInterval;

    // max amount of obstacles that should be instantiated in one generation cycle
    [SerializeField]
    private int obstacleBatchSize;

    public IReadOnlyList<GameObject> Obstacles { get => obstacles; }

    public Vector2Int WorldBounds { get => worldBounds; }

    public Vector2 ObstacleSpawnRange { get => obstacleSpawnRange; }

    public float ReRootInterval { get => reRootInterval; }

    public int ObstacleBatchSize { get => obstacleBatchSize; }
}
