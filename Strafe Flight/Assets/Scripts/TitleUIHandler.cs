using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class TitleUIHandler : MonoBehaviour
{
    const string UNLOCKED_TEXT = "UNLOCKED";
    const string SELECT_TEXT = "SELECT";
    const string BUY_TEXT = "BUY";

    [System.Serializable]
    private struct ColorButtonItemContainer
    {
        public Button button;
        public Text buttonText;
        public TMP_Text costText;
        public StoreItem item;
    }

    [System.Serializable]
    private struct BuyButtonItemContainer
    {
        public Button button;
        public TMP_Text buttonText;
    }

    [SerializeField]
    private BuyButtonItemContainer BuyButton;

    [SerializeField]
    private List<ColorButtonItemContainer> ColorButtons;

    [SerializeField]
    private Button PlayButton;

    [SerializeField]
    private Button NextFlyer;

    [SerializeField]
    private Button PreviousFlyer;

    [SerializeField]
    private Button CustomizeButton;

    [SerializeField]
    private GameObject previewFlyerObject;

    [SerializeField]
    private float previewFlyerRotSpeed;

    [SerializeField]
    TMP_Text CoinsText;

    [SerializeField]
    TMP_Text FlyerPriceText;


    private float previewFlyerRot = 0.0f;

    private void Start()
    {
        PlayButton.onClick.AddListener(LoadGame);
        NextFlyer.onClick.AddListener(NextFlyerPreview);
        PreviousFlyer.onClick.AddListener(PreviousFlyerPreview);
        BuyButton.button.onClick.AddListener(TryPurchaseFlyer);
        InitColorButtons();
        RefreshColorButtons();
        RefreshBuyButton();
        RefreshFlyerPriceText();
        RefreshCustomizeButton();
        FlyerConfigurator.Instance.FlyerSwitchObject(previewFlyerObject, true);
        RefreshFlyerColor();
    }

    private void Update()
    {
        previewFlyerRot += previewFlyerRotSpeed * Time.deltaTime;
        previewFlyerRot %= 360.0f;

        previewFlyerObject.transform.rotation = Quaternion.Euler(new Vector3(0.0f, previewFlyerRot, 0.0f));
        CoinsText.text = $"COINS: {PersistenceManager.Instance.Coins}";
    }


    private void InitColorButtons()
    {
        foreach (var colorButton in ColorButtons)
        {
            colorButton.button.onClick.AddListener(() => TryPurchaseColor(colorButton.item));
        }
    }

    private void RefreshColorButtons()
    {
        foreach (var colorButton in ColorButtons)
        {
            colorButton.buttonText.text = colorButton.item.Name;
            colorButton.costText.text = PersistenceManager.Instance.CheckItemUnlocked(colorButton.item.Name) ? UNLOCKED_TEXT : $"cost: {colorButton.item.Price}";
            colorButton.button.interactable = colorButton.item == FlyerConfigurator.Instance.ActiveColorItem ? false : true;
        }
    }

    private void RefreshBuyButton()
    {
        BuyButton.buttonText.text = PersistenceManager.Instance.CheckItemUnlocked(FlyerConfigurator.Instance.PreviewFlyer.Name) ? SELECT_TEXT : BUY_TEXT;
        BuyButton.button.interactable = FlyerConfigurator.Instance.ActiveFlyerItem == FlyerConfigurator.Instance.PreviewFlyer ? false : true;
    }

    private void RefreshCustomizeButton()
    {
        CustomizeButton.interactable = PersistenceManager.Instance.CheckItemUnlocked(FlyerConfigurator.Instance.PreviewFlyer.Name);
    }

    private void RefreshFlyerPriceText()
    {
        FlyerPriceText.gameObject.SetActive(!PersistenceManager.Instance.CheckItemUnlocked(FlyerConfigurator.Instance.PreviewFlyer.Name));
        FlyerPriceText.text = $"COST: {FlyerConfigurator.Instance.PreviewFlyer.Price}";
    }

    private void RefreshFlyerColor()
    {
        FlyerConfigurator.Instance.FlyerSwitchColor(previewFlyerObject);
    }

    private void NextFlyerPreview()
    {
        if (FlyerConfigurator.Instance.NextPreviewFlyer(true))
        {
            FlyerConfigurator.Instance.FlyerSwitchObject(previewFlyerObject, true);
            PreviousFlyer.interactable = true;
            RefreshFlyerPriceText();
            RefreshBuyButton();
            RefreshCustomizeButton();
            RefreshFlyerColor();
        }
        else
        {
            NextFlyer.interactable = false;
        }
    }

    private void PreviousFlyerPreview()
    {
        if (FlyerConfigurator.Instance.NextPreviewFlyer(false))
        {
            FlyerConfigurator.Instance.FlyerSwitchObject(previewFlyerObject, true);
            NextFlyer.interactable = true;
            RefreshFlyerPriceText();
            RefreshBuyButton();
            RefreshCustomizeButton();
            RefreshFlyerColor();
        }
        else
        {
            PreviousFlyer.interactable = false;
        }
    }

    private void TryPurchaseColor(StoreItem item)
    {
        if (PersistenceManager.Instance.CheckItemUnlocked(item.Name))
        {
            // set color to item
            FlyerConfigurator.Instance.SetNewColor(item);
        }
        else
        {
            if (PersistenceManager.Instance.TryPurchase(item.Price))
            {
                PersistenceManager.Instance.UnlockColor(item.Name);
                FlyerConfigurator.Instance.SetNewColor(item);
            }
        }
        RefreshFlyerColor();
        RefreshColorButtons();
    }

    private void TryPurchaseFlyer()
    {
        if (PersistenceManager.Instance.CheckItemUnlocked(FlyerConfigurator.Instance.PreviewFlyer.Name))
        {
            FlyerConfigurator.Instance.SetNewFlyer(FlyerConfigurator.Instance.PreviewFlyer);
        }
        else
        {
            if (PersistenceManager.Instance.TryPurchase(FlyerConfigurator.Instance.PreviewFlyer.Price))
            {
                PersistenceManager.Instance.UnlockFlyer(FlyerConfigurator.Instance.PreviewFlyer.Name);
                FlyerConfigurator.Instance.SetNewFlyer(FlyerConfigurator.Instance.PreviewFlyer);
            }
        }

        RefreshBuyButton();
        RefreshFlyerPriceText();
        RefreshCustomizeButton();
    }

    private void LoadGame()
    {
        SceneManager.LoadScene("Game");
    }


}
