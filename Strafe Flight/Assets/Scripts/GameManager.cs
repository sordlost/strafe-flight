using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton Definition
    private static GameManager instance;

    public static GameManager Instance { get => instance; }

    private GameManager()
    {

    }
    #endregion

    public enum GameState
    {
        PLAYING,
        OVER
    }

    [SerializeField]
    GameObject playerObject;

    [SerializeField]
    WorldConfig activeWorldConfig;

    [SerializeField]
    WorldManager worldGenerator;

    [SerializeField]
    GameUIHandler UIHandler;

    [SerializeField]
    FlyerController flyerController;

    [SerializeField]
    GameObject mainCamera;

    public GameState gameState = GameState.PLAYING;

    private int score = 0;

    private DateTime startTime;

    public int Score { get => score; }

    public GameObject PlayerObject { get => playerObject; }

    public WorldConfig ActiveWorldConfig { get => activeWorldConfig; }

    public GameObject MainCamera { get => mainCamera; }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        startTime = DateTime.Now;

        InitPlayer();
        flyerController.OnFlyerDestroyed += EndGame;
    }

    private void InitPlayer()
    {
        FlyerConfigurator.Instance.FlyerSwitchObject(playerObject, false);
        FlyerConfigurator.Instance.FlyerSwitchColor(playerObject);
        FlyerConfigurator.Instance.FlyerSwitchConfig(playerObject);
    }

    private void EndGame()
    {
        if (gameState == GameState.PLAYING)
        {
            score = Mathf.FloorToInt((float)(DateTime.Now - startTime).TotalSeconds) * PersistenceManager.Instance.scoreMultiplier;
            UIHandler.ShowScreen();
        }
        gameState = GameState.OVER;
    }
}
