using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mobile
{
    public class GesturesController : MonoBehaviour
    {
        private List<Gesture> gestures = new List<Gesture> { new AccelerateSwipe() };

        private List<Gesture> activeGestures = new List<Gesture>();

        public event Action<Gesture> OnGestureStarted;

        public event Action<Gesture> OnGestureExecuted;

        private void OnEnable()
        {
            OnGestureExecuted += OnGestureExecutedHandler;
        }

        private void Update()
        {
            if (Input.touchCount > 0)
            {
                foreach (Touch touch in Input.touches)
                {
                    foreach (Gesture gesture in gestures)
                    {
                        if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                        {
                            gesture.RemoveTouch(touch.fingerId);
                        }
                        else
                        {
                            gesture.UpdateTouch(touch.fingerId, touch.position);
                        }
                    }
                }

                foreach (Gesture gesture in gestures)
                {
                    if ((activeGestures.Count > 0 && !gesture.IsSimultaneous) || activeGestures.Contains(gesture))
                    {
                        continue;
                    }

                    if (gesture.GestureConditionsMet(Input.touchCount))
                    {
                        activeGestures.Add(gesture);
                        OnGestureStarted?.Invoke(gesture);
                        Debug.LogError("ABILITY FIRED");
                    }
                }
            }
        }

        public void TriggerGestureExecuted(Gesture gesture)
        {
            OnGestureExecuted?.Invoke(gesture);
        }

        public void OnGestureExecutedHandler(Gesture gesture)
        {
            activeGestures.Remove(gesture);
        }
    }
}
