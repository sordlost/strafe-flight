using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Mobile
{
    namespace Controls
    {
        public class JoystickVisualizer : MonoBehaviour
        {
            [SerializeField]
            Image joystickBackgroundImage;

            [SerializeField]
            Image joystickImage;
        }
    }
}
