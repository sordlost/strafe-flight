using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mobile
{
    namespace Controls
    {
        public class JoystickController : MonoBehaviour
        {
            [SerializeField]
            JoystickConfig config;

            Vector2 rootPos;

            // joystick UI screen span
            float screenRadius;

            // how far from the center is the maximum input range (in pixels)
            float inputRadius;

            float inputDeadzone;

            Vector2 touchOriginPos;

            int inputTouchIdx;

            bool inputFound;

            public float XOffset;

            public float YOffset;

            private void OnEnable()
            {
                rootPos = config.ScreenRoot;
                screenRadius = config.ScreenTouchDetectionRadius;
                inputRadius = config.InputTouchRadius;
                inputDeadzone = config.Deadzone;
            }

            private void Update()
            {
                if (Input.touchCount > 0)
                {
                    if (!inputFound)
                    {
                        foreach (var touch in Input.touches)
                        {
                            if (Vector2.Distance(touch.position, rootPos) <= screenRadius)
                            {
                                if (touch.phase == TouchPhase.Began)
                                {
                                    touchOriginPos = touch.position;
                                    inputTouchIdx = touch.fingerId;
                                    inputFound = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (var touch in Input.touches)
                        {
                            if (touch.fingerId == inputTouchIdx)
                            {
                                if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                                {
                                    XOffset = Mathf.Clamp((touch.position.x - touchOriginPos.x)/inputRadius, -1.0f, 1.0f);
                                    YOffset = Mathf.Clamp((touch.position.y - touchOriginPos.y)/inputRadius, -1.0f, 1.0f);

                                    XOffset = Mathf.Abs(XOffset) > inputDeadzone ? XOffset : 0.0f;
                                    YOffset = Mathf.Abs(YOffset) > inputDeadzone ? YOffset : 0.0f;
                                }
                                else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                                {
                                    XOffset = 0.0f;
                                    YOffset = 0.0f;
                                    inputFound = false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
