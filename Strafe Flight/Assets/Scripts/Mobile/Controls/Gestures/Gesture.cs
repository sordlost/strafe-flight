using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mobile
{
    public abstract class Gesture
    {
        // frame span of touch data
        protected const int touchDataLength = 30;
        protected abstract int FingersRequired { get; }
        public abstract bool IsSimultaneous { get; }

        protected Dictionary<int, List<Vector2>> TouchesData = new Dictionary<int, List<Vector2>>();

        public abstract bool GestureConditionsMet(int fingerCount);

        public void UpdateTouch (int id, Vector2 position)
        {
            if (TouchesData.ContainsKey(id))
            {
                if (TouchesData.Count >= touchDataLength)
                {
                    TouchesData[id].RemoveAt(0);
                }
                TouchesData[id].Add(position);
            }
            else
            {
                TouchesData[id] = new List<Vector2>() { position };
            }
        }

        public void RemoveTouch(int id)
        {
            TouchesData.Remove(id);
        }
    }
}
