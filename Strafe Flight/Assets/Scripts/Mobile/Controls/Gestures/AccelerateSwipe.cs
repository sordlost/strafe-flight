using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mobile
{
    public class AccelerateSwipe : Gesture
    {
        protected override int FingersRequired { get => 2; }

        public override bool IsSimultaneous { get => true; }

        private float xTouchThreshold = 5.0f;

        public override bool GestureConditionsMet(int fingerCount)
        {
            if (fingerCount >= FingersRequired)
            {
                List<float> data = new List<float>();

                foreach (List<Vector2> touchPositions in TouchesData.Values)
                {
                    //difference in screen xpos between the last and first recorded position
                    data.Add(touchPositions[touchPositions.Count - 1].x - touchPositions[0].x);
                }

                bool negativeValFound = false;
                bool positiveValFound = false;

                foreach (var value in data)
                {
                    Debug.LogError(value);
                }

                foreach (float record in data)
                {
                    if (Mathf.Abs(record) >= xTouchThreshold )
                    {
                        if (record > 0.0f)
                        {
                            positiveValFound = true;
                        }
                        else if (record < 0.0f)
                        {
                            negativeValFound = true;
                        }
                    }
                }

                return (positiveValFound && negativeValFound);
            }
            else
            {
                return false;
            }
        }
    }
}
