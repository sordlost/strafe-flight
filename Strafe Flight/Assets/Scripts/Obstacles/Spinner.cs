using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    [SerializeField]
    Vector2 rotationSpeedRange;

    float rot = 0.0f;
    float rotSpeed;
    private void OnEnable()
    {
        rotSpeed = Random.Range(rotationSpeedRange.x, rotationSpeedRange.y);
    }

    private void Update()
    {
        rot += rotSpeed * Time.deltaTime;
        rot %= 360.0f;

        transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, rot));
    }
}
