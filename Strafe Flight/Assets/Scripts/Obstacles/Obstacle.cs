using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Obstacle : MonoBehaviour
{
    private WorldConfig worldConfig;

    [SerializeField]
    private float colliderSizeOffset = 1.0f;

    public event Action<Obstacle> OnObstacleCrossed;

    private void OnEnable()
    {
        worldConfig = GameManager.Instance.ActiveWorldConfig;
        BoxCollider crossedTriggerCollider = GetComponent<BoxCollider>();
        crossedTriggerCollider.size = new Vector3(worldConfig.WorldBounds.x + colliderSizeOffset, worldConfig.WorldBounds.y + colliderSizeOffset, 1.0f);
        crossedTriggerCollider.center = gameObject.transform.InverseTransformPoint(new Vector3(0.0f, 0.0f, gameObject.transform.position.z));
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            OnObstacleCrossed?.Invoke(this);
        }
    }
}
