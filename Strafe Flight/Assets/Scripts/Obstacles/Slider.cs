using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slider : MonoBehaviour
{

    [SerializeField]
    bool horizontal;

    [SerializeField]
    GameObject leftPlane;

    [SerializeField]
    GameObject rightPlane;

    [SerializeField]
    float cycleSpeed;

    WorldConfig worldConfig;

    float xl, xr = 0.0f;
    float lPlanePos, rPlanePos = 0.0f;
    float worldDim;

    Vector3 lTargetPos;
    Vector3 rTargetPos;

    private void OnEnable()
    {
        worldConfig = GameManager.Instance.ActiveWorldConfig;
        worldDim = horizontal ? worldConfig.WorldBounds.x / 2.0f : worldConfig.WorldBounds.y / 2.0f;
    }

    private void Update()
    {
        xl += Time.deltaTime * cycleSpeed;
        xr += Time.deltaTime * cycleSpeed;

        xl %= 2 * Mathf.PI;
        xr %= 2 * Mathf.PI;

        lPlanePos = Mathf.Sin(xl) * worldDim;
        rPlanePos = -Mathf.Sin(xr) * worldDim;

        lTargetPos = leftPlane.transform.position;
        rTargetPos = rightPlane.transform.position;

        if (horizontal)
        {
            lTargetPos.x = lPlanePos;
            rTargetPos.x = rPlanePos;
        }
        else
        {
            lTargetPos.y = lPlanePos;
            rTargetPos.y = rPlanePos;
        }

        leftPlane.transform.position = lTargetPos;
        rightPlane.transform.position = rTargetPos;
    }
}
