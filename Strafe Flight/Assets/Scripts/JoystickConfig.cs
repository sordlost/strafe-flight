using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Mobile
{
    namespace Controls
    {
        [CreateAssetMenu(fileName = "Joystick Config", menuName = "Scriptable Objects/JoystickConfig", order = 1)]
        public class JoystickConfig : ScriptableObject
        {
            [SerializeField]
            Image backgroundImage;

            [SerializeField]
            Image joyImage;

            [SerializeField]
            Vector2 screenRoot;

            [SerializeField]
            float screenTouchDetectionRadius;

            [SerializeField]
            float inputTouchRadius;

            [SerializeField]
            float deadzone;

            public Image BackgroundImage { get => backgroundImage; }

            public Image JoyImage { get => joyImage; }

            public Vector2 ScreenRoot { get => screenRoot; }

            public float ScreenTouchDetectionRadius { get => screenTouchDetectionRadius; }

            public float InputTouchRadius { get => inputTouchRadius; }

            public float Deadzone { get => deadzone; }
        }
    }
}
