using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Abilities Config", menuName = "Scriptable Objects/AbilitiesConfig", order = 1)]
public class AbilitiesConfig : ScriptableObject
{
    [Header("Accelerate Ability")]
    [SerializeField]
    private float accVelocityBoost;
    [SerializeField]
    private float accTimespan;

    public float AccVelocityBoost { get => accVelocityBoost; }
    public float AccTimespan { get => accTimespan; }
}
