using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Flyer", menuName = "Scriptable Objects/Flyer", order = 1)]
public class Flyer : ScriptableObject
{
    [SerializeField]
    private float maxVelocityForward;

    [SerializeField]
    private float maxVelocityHorizontal;

    [SerializeField]
    private float maxVelocityVertical;

    [SerializeField]
    private float dampingRotX;

    [SerializeField]
    private float dampingRotY;

    [SerializeField]
    private float dampingRotZ;

    [SerializeField]
    private float dampingVelX;

    [SerializeField]
    private float dampingVelY;

    [SerializeField]
    private float dampingVelZ;

    [SerializeField]
    private float mass;

    [SerializeField]
    private Vector3 turningBounds;

    public float MaxVelocityForward { get => maxVelocityForward; }
    public float MaxVelocityHorizontal { get => maxVelocityHorizontal; }
    public float MaxVelocityVertical { get => maxVelocityVertical; }
    public float DampingRotX { get => dampingRotX; }
    public float DampingRotY { get => dampingRotY; }
    public float DampingRotZ { get => dampingRotZ; }
    public float DampingVelX { get => dampingVelX; }
    public float DampingVelY { get => dampingVelY; }
    public float DampingVelZ { get => dampingVelZ; }
    public float Mass { get => mass; }
    public Vector3 TurningBounds { get => turningBounds; }
}
